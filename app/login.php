<?php
require_once '../class/Cfg.php';
$user = new User();
$tabErreur = [];

if (filter_input(INPUT_POST, 'submit')) {
    $user->log = filter_input(INPUT_POST, 'log', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $user->mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

    if (!$user->log) {
        $tabErreur[] = I18n::get('FORM_ERR_LOGIN');
    }
    if (!$user->mdp) {
        $tabErreur[] = I18n::get('FORM_ERR_MDP');
    }

    // On lance la création du User après vérif variable
    if (!$tabErreur && $user->login()) {
        header("Location:accueil.php");
        exit;
    }


}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Se connecter</title>
    <link href="../css/flowers.css" rel="stylesheet" type="text/css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="180x180" href="../favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
    <link rel="manifest" href="../favicon/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


</head>
<body>
<?php require_once '../inc/header.php'; ?>

<div class="row">
    <div class="col">
        <div id="container">
            <h3 >Se connecter</h3>
            <form style="margin-top: 80px;"name="form1" method="post" action="login.php" >
                <div class="item"><label>Log</label>
                    <input type="text" name="log" maxlength="50" class="nom" required="required"/>
                </div>
                <div class="item"><label>MDP</label>
                    <input type="password" name="mdp" maxlength="10" size="10" class="ref" required="required" />
                </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Se connecter"/>
<!--                <button type="button" class="btn btn-light"> <a href="accountCreate.php">Pas encore de compte ?</a></button>-->
            </form>
        </div>
    </div>
</div>


</body>
</html>
