<?php
require_once '../class/Cfg.php';

if (!Cfg::$user) {
    header('Location:login.php');
    exit;
}

$fleur = new Fleur();
$cnx = Connexion::getInstance();
//const TAB_EXT = [];
//const TAB_MIME = ['image/jpeg', 'image/png'];
//const IMG_LARGEUR = 300;
//const IMG_HAUTEUR = 300;
if (filter_input(INPUT_POST, 'submit')) {
    $fleur->nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $fleur->remarques = filter_input(INPUT_POST, 'remarques', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $fleur->liens = filter_input(INPUT_POST, 'liens', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $upload = new Upload('photo', TAB_EXT, TAB_MIME);
    if (!$fleur->nom) {
       echo ("Merci de saisir un nom");
    }

    if (!$fleur->remarques) {
        echo ("Merci de saisir une description");
    }

    if($fleur->nom && $fleur->remarques && $fleur->liens) {
        $fleur->sauver();
        header("Location:accueil.php");
        exit;
    }
}
//$tabErreur = [];
//$IdImgUpload = $fleur->id_fleur;
//
//if ($upload->codeErreur === 4) {
//    $cnx->commit();
//    header("location:index.php");
//    exit;
//}
//$tabErreur = array_merge($tabErreur, $upload->tabErreur);
//if (!$upload->tabErreur) {
//    //traitement image
////    var_dump($upload);
//    $image = new Image($upload->cheminServeur);
////    var_dump($image);
//    if (!$image->tabErreur) {
//        //traitement copie
//        $tabErreur = array_merge($tabErreur, $image->tabErreur);
//        $image->copier(Cfg::IMG_V_LARGEUR, Cfg::IMG_V_HAUTEUR, "../img/prod_{$IdImgUpload}_v.jpg");
//        $image->copier(Cfg::IMG_P_LARGEUR, Cfg::IMG_P_HAUTEUR, "../img/prod_{$IdImgUpload}_p.jpg");
//        $tabErreur = array_merge($tabErreur, $image->tabErreur);
//    }
//}
//$idImg = file_exists("../img/prod_{$fleur->id_fleur}_v.jpg") ? $fleur->id_fleur : 0;
////var_dump($image->tabErreur);
?>




<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ajouter une plante</title>
    <script src="../js/index.js" type="text/javascript"></script>
    <script src="../js/accueil.js" type="text/javascript"></script>
    <script src="../js/editer.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="../css/flowers.css" rel="stylesheet" type="text/css"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="../favicon/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!--    <script>-->
<!--        const TAB_EXT = JSON.parse(`<?//= json_encode(Cfg::TAB_EXT) ?>//`); // on déjsonise (JSON.parse) ce qu'on a jsonisé avec json_encode
//        const TAB_MIME = JSON.parse(`<?//= json_encode(Cfg::TAB_MIME) ?>//`);
//        const MAX_FILE_SIZE = <?//= Upload::maxFileSize() ?>//; // on passe des variables de php à JS
//    </script>-->
</head>
<body>
<?php require_once '../inc/header.php'; ?>

<div class="row">
    <div class="col">
        <div id="container">
            <h3>Ajouter une plante</h3>
            <form name="form1" method="post" action="ajouterPlante.php" enctype="multipart/form-data">
                <div class="item"><label>Nom de la plante</label>
                    <input type="text" name="nom" maxlength="50" class="nom" required="required"/>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description </label>
                    <textarea class="form-control" name="remarques" rows="3" required="required" ></textarea>
                </div>
<!--                <div class="form-group">-->
<!--                    <div class="item">-->
<!--                        <label></label>-->
<!--                        <label>Uploader une photo </label>-->
<!--                        <input type="file" id="photo" name="photo" onchange="afficherPhoto(this.files)"/> files est un tableau contenant les chemins des fichiers choisis -->
<!--                        <input type="button"  value="Parcourir..." onclick="document.form1.photo.click()">  ou alors this.form.photo.click()-->
<!--                    </div>-->
<!--                    <div id="vignette" style="background-image: url(../img/prod_<?//= $idImg ?>/*_v.jpg?alea=*/<?//= rand() ?>/*)">rand() — Génère une valeur aléatoire, empêche le navigateur de stocker l'img en cache -->
                <!--    </div>
                    </div>-->
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Liens </label>
                    <textarea class="form-control" name="liens" rows="3" ></textarea>
                </div>
                <input type="submit" class="btn btn-outline-primary" name="submit" value="Sauvegarder" />
                <button type="button" class="btn btn-warning"> <a href="accueil.php">Revenir en arrière</a></button>
            </form>
        </div>
    </div>
</div>




</body>
</html>
