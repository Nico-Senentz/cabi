<?php
require_once 'class/Cfg.php';
$user = new User();

if (filter_input(INPUT_POST, 'submit')) {
    $user->log = filter_input(INPUT_POST, 'log', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $user->mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $user->nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $user->prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

    // On lance la création du User après vérif variable
    $user->sauver();

}

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Créer un compte</title>
    <link href="../css/flowers.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header></header>
<div id="container">
    <div class="categorie"><h3>Mon compte</h3></div>
    <form name="form1" method="post" action="accountCreate.php" >
        <div class="item"><label>Nom</label>
            <input type="text" name="nom" maxlength="50" class="nom" required="required"/>
        </div>
        <div class="item"><label>Prénom</label>
            <input type="text" name="prenom" maxlength="10" size="10" class="ref" required="required" />
        </div>
        <div class="item"><label>Choisissez un nom d'utilisateur</label>
            <input type="text" name="log" maxlength="20" size="10" class="ref" required="required" />
        </div>
        <div class="item"><label>Choisissez votre mot de passe</label>
            <input type="password" name="mdp" maxlength="20" size="10" class="ref" required="required" />
        </div>
        <input type="submit" name="submit" value="submit"/>
    </form>

    <a href="login.php">Revenir en arrière</a>

</div>
</body>
</html>

