<?php
require_once '../class/Cfg.php';


if (!Cfg::$user) {
    header('Location:login.php');
    exit;
}
$fleur = new Fleur();
$allFlowers = $fleur->tous();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>I love flowers (and plants)</title>
    <script src="../js/index.js" type="text/javascript"></script>
    <script src="../js/accueil.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="../css/flowers.css" rel="stylesheet" type="text/css"/>
    <link rel="apple-touch-icon" sizes="180x180" href="../favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
    <link rel="manifest" href="../favicon/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>
<body>

<?php require_once '../inc/header.php'; ?>
<div class="row">
    <div class="col"><div class="jumbotron jumbotron-fluid">
            <div class="container" style="margin-top: 30px;">
                <h1 class="display-5">Ma collection de plantes et fleurs</h1>
                <p class="lead"><?php

                    if(!$allFlowers) {
                    ?>
                <div class="alert alert-info" role="alert">
                    Il n'y a pas de plantes dans la collection ;)
                </div>
                <?php
                }
                ?></p>
            </div>
            <div class="container">
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="ajouterPlante.php" role="button">Ajouter une plante</a>
                </p>
            </div>

        </div></div>
</div>
<div class="row">
    <div class="col">
        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">NOM</th>
                    <th scope="col">DESCRIPTION</th>
<!--                    <th scope="col"></th>-->
                    <th scope="col">Liens</th>
                    <th scope="col">Modifier/supprimer</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($allFlowers as $fleur) {
                ?>
                <form action="modifierPlante.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_fleur" value="<?= $fleur->id_fleur ?>"/>
                    <tr>
                        <th><?= $fleur->nom ?></th>
                        <th> <?= $fleur->remarques ?></th>
<!--                        <th>-->
<!--                            --><?php
//                            $idImg = file_exists("../img/prod_{$fleur->id_fleur}_v.jpg") ? $fleur->id_fleur : 0;
//                            ?>
<!--                            <div class="blocProduit">-->
<!--                          <div class="blocProduit" onclick="detailProduit(<?//= $fleur->id_fleur ?>)">-->
<!--                               <img src="../img/prod_<?//= $idImg ?>_v.jpg?alea= --><?//= rand() ?><!--"/>-->
<!--                            </div>-->
<!--                        </th>-->
                        <th><?= $fleur->liens ?> </th>
                        <th>
                            <input class="btn btn-outline-primary" type="submit" value="Modifier"></th>

                    </tr>

                </tbody>
                </form>
                <?php
                }
                ?>
            </table>
        </div>

    </div>
</div>

        </body>
        </html>
