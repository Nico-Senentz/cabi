function detailProduit(id_produit){
	location = `detail.php?id_produit=${id_produit}`;
}
// function ajouterProduit(id_categorie){
//     location = `editer.php?id_categorie=${id_categorie}`;
// }

function modifierProduit(evt,id_produit){
    evt.stopPropagation();
    location = `editer.php?id_produit=${id_produit}`;
}

// function supprimerPlante(evt, id_fleur){
//     evt.stopPropagation();
//     console.log(id_fleur);
//     //Requête AJAX en POST
//     if (confirm("Vraiment supprimer ? ")){
//         let url = 'supprimer.php?id_fleur=' + id_fleur;
//       fetch(url)
//               .then (response => {
//                   if (response.ok) // reponse 200
//               location.reload();
//               })
//                       .catch(error => console.error(error));
//     }
// }

function supprimerImage(evt, id_produit){
    evt.stopPropagation();
    //Requête AJAX en POST
    if (confirm("Vraiment supprimer ? ")){
        let url = 'supprimerImage.php?id_produit='+id_produit;
      fetch(url)
              .then (response => {
                  if (response.ok) // reponse 200
              location.reload();
              })
                      .catch(error => console.error(error));
    }
}

