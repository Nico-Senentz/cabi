// onchange de l'upload de photo
function afficherPhoto(files) {
    var vignette = document.querySelector('#vignette');
    vignette.style.backgroundImage = '';   // on dégage le background de la vignette
    if (!files || !files.length)
        return;
    var file = files[0]; // on récupère l'image dans le tableau files à l'indice 0
    if (!file.size)  // si taille file = 0 donc null
        return alert("ERROR : empty file !!!");
    if (file.size > MAX_FILE_SIZE)
        return alert("ERROR : file too big !!!");
    
    var ext = file.name.split('.').pop(); // obtenir l'extension du fichier (pop retourne la dernière case du tableau)
   if(TAB_EXT.length && !TAB_EXT.includes(ext)) // vérifier l'extension du fichier : si on a un TAB_EXT 
       return alert("ERROR : file extension not allowed !!!");
   
   if(TAB_MIME.length && !TAB_MIME.includes(file.type))
       return alert("ERROR : file MIME type not allowed !!!");
   
   var reader = new FileReader();
   reader.onload = function() {
       vignette.style.backgroundImage = `url(${this.result})`;    //   this.result est  
   };
   reader.readAsDataURL(file);
};



function annuler(id_produit = 0) {
    document.querySelector('form').reset();
    document.querySelector('.erreur').innerHTML = "";
    document.querySelector('#vignette').style.backgroundImage = `url(../img/prod_${id_produit}_v.jpg)`;
    
}







