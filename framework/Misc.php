<?php

    class Misc {

        private function __construct() {
            // 100% statique : classe fourre-tout
        }

        // Function pour crypter les mdp de l'intégralité d'une BDD
        public static function crypterMdp($table, $colPk, $colMdp) {
            $cnx = Connexion::getInstance();
            $req = "SELECT * FROM {$table} WHERE {$colMdp} IS NOT NULL";
            $tab = $cnx->xeq($req)->tab();
            foreach ($tab as $obj) {
                $req = "UPDATE {$table} SET {$colMdp}={$cnx->esc(password_hash($obj->$colMdp, PASSWORD_DEFAULT))} WHERE {$colPk}={$obj->$colPk}";
                $cnx->xeq($req);
            }
        }

    }
