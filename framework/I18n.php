<?php

    class I18n {

        private function __construct() {
// classe 100% statique donc pas possible de New I18n
        }

        public static function get($message) {
            $langues = filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE', FILTER_SANITIZE_STRING);
            $langue = $langues ? mb_substr($langues, 0, 2) : 'en';  // recupere le 'fr' de la langue grace à substr et nb_ pour l'utf8(commence a zero et 2 premieres lettres) sinon 'en'
            if (isset(self::$msg[$langue][$message])) {   // si messsage existe langue prefere utilsateur
                return self::$msg[$langue][$message];
            } // on lui retourne en français
            if (isset(self::$msg['en'][$message])) {   // si messsage n'existe pas langue prefere utilsateur on lui retourne
                return self::$msg['en'][$message];
            } // sinon on lui retourne dans la 2eme lanque, l'anglais
            return null;  // sinon retourne null
        }

        private static $msg = [// tableau associatif en 2 dimension (voir shema)
                    'fr' => ['FORM_ERR_CATEGORY' => "La catégorie est absente ou invalide",
                                'FORM_ERR_NAME' => "Le nom est absent ou invalide",
                                'FORM_ERR_REF' => "La ref est absente ou invalide",
                                'FORM_ERR_PRICE' => "Le prix est absent ou invalide",
                                'FORM_ERR_LOG' => "Login absent ou invalide",
                                'FORM_ERR_LOGIN' => "Connexion impossible",
                                'FORM_ERR_MDP' => "Mot de passe absent ou invalide",
                                'FORM_LABEL_CATEGORY' => "Catégorie",
                                'FORM_LABEL_NAME' => "Nom",
                                'FORM_LABEL_REF' => "Référence",
                                'FORM_LABEL_PRICE' => "Prix",
                                'FORM_LABEL_CANCEL' => "Annuler",
                                'FORM_LABEL_SUBMIT' => "Valider",
                                'FORM_LABEL_LOG' => "Pseudo",
                                'FORM_LABEL_MDP' => "Mot de passe",
                                'FORM_LABEL_CONNECT' => " SeConnecter",
                                'UPLOAD_ERR' . UPLOAD_ERR_INI_SIZE => "taille dépasse maxi côté serveur",
                                'UPLOAD_ERR' . UPLOAD_ERR_FORM_SIZE => "taille dépasse maxi côté client",
                                'UPLOAD_ERR' . UPLOAD_ERR_PARTIAL => "fichier partiellement uploadé",
                                'UPLOAD_ERR' . UPLOAD_ERR_NO_FILE => "aucun fichier uploadé",
                                'UPLOAD_ERR' . UPLOAD_ERR_NO_TMP_DIR => "dossier temporaire absent",
                                'UPLOAD_ERR' . UPLOAD_ERR_CANT_WRITE => "dossier temporaire inaccessible",
                                'UPLOAD_ERR' . UPLOAD_ERR_EXTENSION => "une extension PHP a bloqué l'upload",
                                'UPLOAD_ERR_EMPTY' => "fichier vide",
                                'UPLOAD_ERR_EXTENSION' => "l'extension du fichier est invalide",
                                'UPLOAD_ERR_MINE' => "le type mine du fichier est invalide",
                                'UPLOAD_ERR_MOVE' => "erreur au déplacement",
                                'IMG_ERR_UNAVAILABLE' => "image inutilisable",
                                'IMG_ERR_TYPE' => "type image invalide",
                                'IMG_ERR_CANT_WRITE' => "Copie image impossible",
                                'IMG_ERR_OUT_OF_MEMORY' => "Mémoire insuffisante",
                                'DB_ERR_DSN_ALREADY_SET' => "Already set",
                                'DB_ERR_CONNECTION_FAILED' => "Connexion impossible",
                                'DB_ERR_DSN_UNDEFINED' => "DSN non défini",
                                'DB_ERR_BAD_REQUEST' => "Requête incorrecte"],
                    // language anglais
                    'en' => ['FORM_ERR_CATEGORY' => "Category empty or invalid",
                                'FORM_ERR_NAME' => "Name empty or wrong",
                                'FORM_ERR_REF' => "Reference empty, invalid or already used",
                                'FORM_ERR_PRICE' => "Price empty or invalid",
                                'FORM_ERR_LOG' => "Login empty or invalid",
                                'FORM_ERR_MDP' => "Password empty or invalid",
                                'FORM_ERR_LOGIN' => "Connection failed",
                                'FORM_LABEL_CATEGORY' => "Category",
                                'FORM_LABEL_NAME' => "Name",
                                'FORM_LABEL_REF' => "Reference",
                                'FORM_LABEL_PRICE' => "Price",
                                'FORM_LABEL_CANCEL' => "Cancel",
                                'FORM_LABEL_SUBMIT' => "Submit",
                                'FORM_LABEL_LOG' => "Login",
                                'FORM_LABEL_MDP' => "Password",
                                'FORM_LABEL_CONNECT' => "Connection",
                                'UPLOAD_ERR' . UPLOAD_ERR_INI_SIZE => "size too much server side",
                                'UPLOAD_ERR' . UPLOAD_ERR_FORM_SIZE => "size exceeds maxi client side",
                                'UPLOAD_ERR' . UPLOAD_ERR_PARTIAL => "partially uploaded file",
                                'UPLOAD_ERR' . UPLOAD_ERR_NO_FILE => "no uploaded file",
                                'UPLOAD_ERR' . UPLOAD_ERR_NO_TMP_DIR => "Temporary folder not present",
                                'UPLOAD_ERR' . UPLOAD_ERR_CANT_WRITE => "temporary folder inaccessible",
                                'UPLOAD_ERR' . UPLOAD_ERR_EXTENSION => "a PHP extension prevents the upload from being completed",
                                'UPLOAD_ERR_EMPTY' => "empty file",
                                'UPLOAD_ERR_EXTENSION' => "wrong file extension",
                                'UPLOAD_ERR_MINE' => "wrong MINE extension",
                                'UPLOAD_ERR_MOVE' => "Can't save the file",
                                'IMG_ERR_UNAVAILABLE' => "image unavailable",
                                'IMG_ERR_TYPE' => "wrong image size",
                                'IMG_ERR_CANT_WRITE' => "Copie image impossible",
                                'IMG_ERR_OUT_OF_MEMORY' => "Mémoire insuffisante",
                                'DB_ERR_DSN_ALREADY_SET' => "Already set",
                                'DB_ERR_CONNECTION_FAILED' => "Connection failed",
                                'DB_ERR_DSN_UNDEFINED' => "DSN undefined",
                                'DB_ERR_BAD_REQUEST' => "Bad request"],
        ];

    }
