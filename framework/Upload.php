<?php

    class Upload {

        public $nomChamp;       // Nom du champ INPUT
        public $tabExt = [];        // Extensions autorisées (ex : ['jpg', 'jpeg']
        public $tabMIME = [];       // Types MIME autorisés (ex : ['image/jpeg']
        public $nomClient;      // Nom du fichier côté client
        public $extension;      // Extension du fichier sans le point
        public $cheminServeur;      // Chemin du fichier temporaire côté serveur
        public $codeErreur;         // Eventuel code d'erreur
        public $octets;         // Nb d'octets téléchargés
        public $typeMIME;       // Type MIME du fichier
        public $tabErreur = [];         // Complète si pb

        // Si un paramètre est optionnel, lui mettre une valeur par défaut, ici []

        public function __construct($nomChamp, $tabExt = [], $tabMIME = []) {
            $this->nomChamp = $nomChamp;
            $this->tabExt = $tabExt;
            $this->tabMIME = $tabMIME;

            if (!isset($_FILES[$nomChamp]))   // sécurité pr vérifier que $_FILES[$nomChamp] existe, =  si cette clé n'est pas définie
                return;

            $this->nomClient = $_FILES[$nomChamp]['name'];     // $nomChamp est la clé de la 1ère dimension du tableau
            $this->cheminServeur = $_FILES[$nomChamp]['tmp_name'];
            $this->extension = (new SplFileInfo($this->nomClient))->getExtension();    //Récupère l'extension d'un fichier côté serveur    
            $this->codeErreur = $_FILES[$nomChamp]['error'];
            $this->octets = $_FILES[$nomChamp]['size'];
            $this->typeMIME = $_FILES[$nomChamp]['type'];

            if ($this->codeErreur)// si codeErreur différent de 0 pr empêcher que l'erreur  ERR_OK entre dans tabErreur
                $this->tabErreur[] = I18n::get('UPLOAD_ERR_' . $this->codeErreur);

            if (!$this->octets) // si fichier est vide
                $this->tabErreur[] = I18n::get('UPLOAD_ERR_EMPTY');

            if ($tabExt && !in_array($this->extension, $tabExt))      //    in_array — Indique si une valeur appartient à un tableau (tjs demander s'il est rempli)
                $this->tabErreur[] = I18n::get('UPLOAD_ERR_EXTENSION');

            if ($tabMIME && !in_array($this->typeMIME, $tabMIME))      //    in_array — Indique si une valeur appartient à un tableau (tjs demander s'il est rempli dans un if)
                $this->tabErreur[] = I18n::get('UPLOAD_ERR_MIME');
        }

        public function sauver($chemin) {           // déplace le fichier de son emplacement temporaire et le sauve dans $chemin
            if (!move_uploaded_file($this->cheminServeur, $chemin))         // $chemin est la destination
             $this->tabErreur[] = I18n::get('UPLOAD_ERR_MOVE');                                         
        }

        public static function maxFileSize($enOctets = true) {
           $kmg = ini_get('upload_max_filesize');       // ini_get lit la valeur d'une option de configuration
           if(!$enOctets){
               return $kmg;
           }
           
         $strPoids = str_ireplace('G', '*1024**3+', str_ireplace('M', '*1024**2+', str_ireplace('K', '*1024+', $kmg))).'0'; //retourne une chaîne dont ttes les occurrences de G, M et K ds $kmg ont été remplacées par les calculs
         eval("\$poids = {$strPoids};");        //évaluation de l'expression 
         return $poids;
        }

    }
       //  var_dump(Upload::maxFileSize());