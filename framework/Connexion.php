<?php

    require_once 'I18n.php';

    class Connexion {  //

        private static $instance;  //(instance unique)
        private static $DSN;        //(DSN)
        private static $log;           //(identifiant utilisateur)
        private static $mdp;        //(mot de passe)
        private static $opt = [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];            //(options de connexion)
        private $db;                        //(instance de PDO)
        private $jeu;                       //(recordset après une requête SELECT)
        private $nb;                        //(nombre de lignes affectées par la dernière requête)

        private function __construct() {
            //construct privé pour empecher de faire un new Connexion
            // vérifie la présence du DSN et tente la connexion.
            if (!self::$DSN)
                exit(I18n::get('DB_ERR_DSN_UNDEFINED'));
            try {
                $this->db = new PDO(self::$DSN, self::$log, self::$mdp, self::$opt);
            } catch (PDOException $e) {
                echo I18n::get('DB_ERR_CONNECTION_FAILED');
                exit(" : {$e->getMessage()}");
            }
        }

        public static function getInstance() {  //i retourne une instance unique de Connexion.
            if (!self::$instance)   // si $instance est null
                self::$instance = new Connexion();
            return self::$instance;
        }

        public static function setDSN($dbName, $log, $mdp, $host = 'mysql-nicosenentz.alwaysdata.net') {   //définit le DSN
            if (self::$DSN)
                exit(I18n::get('DB_ERR_DSN_ALREADY_SET'));
            self::$DSN = "mysql:dbname={$dbName};host={$host};charset=utf8mb4";
            self::$log = $log;
            self::$mdp = $mdp;
        }

        public function esc($val) {    //retourne $val protégée (échappée)
            return $val === null ? 'NULL' : $this->db->quote($val);
        }

        public function xeq($req) {       //Exécute la requête selon son type (SELECT ou autre).
            try {
                if (mb_stripos(trim($req), 'SELECT') === 0) {
                    $this->jeu = $this->db->query($req);
                    $this->nb = $this->jeu->rowCount();
                } else {
                    $this->jeu = null;
                    $this->nb = $this->db->exec($req);
                }
            } catch (PDOException $e) {
                echo I18n::get('DB_ERR_BAD_REQUEST');
                exit(" : {$req} ({$e->getMessage()})");
            }
            return $this;
        }

        public function nb() {    //Retourne la valeur de la propriété $nb, le nb de lignes affectées
            return $this->nb;
        }

        public function tab($classe = 'stdClass') {          //Retourne un tableau $jeu contenant les enregistrements retrouvés
            if (!$this->jeu)
                return [];
            $this->jeu->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $classe);
            return $this->jeu->fetchAll();
        }

        public function prem($classe = 'stdClass') {    //Retourne le premier enregistrement retrouvé sous forme d'une instance de la classe $classe.
            if (!$this->jeu)
                return null;
            $this->jeu->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $classe);
            return $this->jeu->fetch();
        }

        public function ins($obj) {  //Charge $obj avec le premier enregistrement retrouvé.
            if (!$this->jeu)
                return false;
            $this->jeu->setFetchMode(PDO::FETCH_INTO, $obj);
            return $this->jeu->fetch();
        }

        public function pk() {         //Retourne la dernière PK auto-incrémentée.
            return $this->db->lastInsertId();
        }

        public function start() {     //Exécute une requête START TRANSACTION
            return $this->db->beginTransaction();
        }

        public function savepoint($label) {   // Exécute une requête SAVEPOINT $label
            $req = "SAVEPOINT {$label}";
            $this->xeq($req);
        }

        public function rollback($label = null) {     // Exécute une requête ROLLBACK ou ROLLBACK TO $label
            return $label ? $this->db->rollBackTo($label) : $this->db->rollBack();
        }

        public function commit() {      //Exécute une requête COMMIT
            return $this->db->commit();
        }

    }
