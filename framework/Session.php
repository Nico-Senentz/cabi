<?php

    class Session implements Databasable, SessionHandlerInterface {

        public $sid;   // PHPSESSID
        public $data;   // Données (sérialisées et désérialisées automatiquement par PHP)
        public $date_maj;  // Date de mise à jour (auto)
        private $timeout;   // Timeout (s), aucun si null

        public function __construct($timeout = null) {
            $this->timeout = $timeout;
        }

        public function charger() {             // éviter de charger des sessions périmées, on fait intervenir le time out, date_maj est encore null dc pas de $this->date_maj
            $cnx = Connexion::getInstance();
            $req = "SELECT * FROM session WHERE sid ={$cnx->esc($this->sid)}" . ($this->timeout ?
                                          " AND TIMESTAMPDIFF(SECOND, date_maj, NOW())  < {$this->timeout}" : '');
            return $cnx->xeq($req)->ins($this);
        }

        public function sauver() { // le sid est systématiquement connu
            $cnx = Connexion::getInstance();
            $req = "INSERT INTO session VALUES ({$cnx->esc($this->sid)},{$cnx->esc($this->data)}, DEFAULT) ON DUPLICATE KEY UPDATE data={$cnx->esc($this->data)},date_maj=DEFAULT";
            $cnx->xeq($req);
            return $this;
        }

        public function supprimer() {
            $cnx = Connexion::getInstance();
            $req = "DELETE FROM session WHERE sid={$cnx->esc($this->sid)}";
            return (bool) $cnx->xeq($req)->nb();
        }

        public static function tab($where = 1, $orderBy = 1, $limit = null) {
            // INUTILE ds Session
            return [];
        }

        // METHODES DE SessionHandlerInterface
        public function close() {   //Ferme la session : ferme un fichier
            return true;
        }

        public function destroy($session_id) {  //Détruit une session et ses variables
            $this->sid = $session_id;
            return $this->supprimer();
        }

        public function gc($maxlifetime) {  //Nettoie les vieilles sessions, gc=garbage collector (nettoie la BDD)
            if (!$this->timeout)
                return true;
            $req = "DELETE FROM session WHERE TIMESTAMPDIFF(SECOND, date_maj, NOW()) > {$this->timeout}";
            return (bool) Connexion::getInstance()->xeq($req)->nb(); //nb() retourne le nb de lignes affectées
        }

        public function open($save_path, $name) {  // Initialise la session : ouvre un fichier
            return true;
        }

        public function read($session_id) {  //Lit les données de session
            $this->sid = $session_id;
            return $this->charger() ? $this->data : '';
        }

        public function write($session_id, $session_data) {//  Ecrit les données de session
            $this->sid = $session_id;
            $this->data = $session_data;
            $this->sauver();
            return true;
        }

    }
