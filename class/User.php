<?php

    class User implements Databasable {

        public $id_user;
        public $log;
        public $mdp;
        public $nom;
        public $prenom;

        public function __construct($id_user = null, $log = null, $mdp = null, $nom = null, $prenom = null) {
            $this->id_user = $id_user;
            $this->log = $log;
            $this->mdp = $mdp;
            $this->nom = $nom;
            $this->prenom = $prenom;
        }

        public function charger() {
            if (!$this->id_user)
                return false;
            $req = "SELECT * FROM user WHERE id_user ={$this->id_user}"; // requete SQL qui permet de recupérer un user grace a son, id_user
            return Connexion::getInstance()->xeq($req)->ins($this);
        }

        public function sauver() {
            $cnx = Connexion::getInstance();
            if ($this->id_user) {
                //si l'user a un id_user alors on fait un UPDATE du user en BDD
                $req = "UPDATE user SET log={$cnx->esc($this->log)},mdp={$cnx->esc($this->mdp)},nom={$cnx->esc($this->nom)},prenom={$cnx->esc($this->prenom)} WHERE id_user={$this->id_user}";
                $cnx->xeq($req);
            } else { // si l'user n'a pas d id_user alors on fait un INSERT du nouveau user en BDD en passant tout les parametres
                $req = "INSERT INTO user VALUES (DEFAULT,{$cnx->esc($this->log)},{$cnx->esc($this->mdp)},{$cnx->esc($this->nom)},{$cnx->esc($this->prenom)})";
                $this->id_user = $cnx->xeq($req)->pk();
            }
            // le DEFAULT de la requete INSERT permet d'utiliser l'auto incrementation de l'id_user de la BDD
            return $this;
        }

        public function supprimer() {
            if (!$this->id_user)
                return false;
            $req = "DELETE FROM user WHERE id_user={$this->id_user}";
            return (bool) Connexion::getInstance()->xeq($req)->nb();
        }

        public static function tab($where = 1, $orderBy = 1, $limit = null) {
            $req = "SELECT * FROM user WHERE {$where} ORDER BY {$orderBy}" . ($limit ? " LIMIT {$limit}" : '');
            return Connexion::getInstance()->xeq($req)->tab(__CLASS__);
        }

        public function login() {// suppose les propriétés log et mdp renseignées, utilise password verify() et retourne true ou false selon que l'user peut être logué ou pas
            $_SESSION['id_user'] = null;
            if (!($this->log || $this->mdp))  // si pas de log ou de mdp
                return false;
            $mdp = $this->mdp;   //this->mdp est maintenant le mdp en clair
            $cnx = Connexion::getInstance();
            $req = "SELECT * FROM user WHERE log={$cnx->esc($this->log)} AND mdp={$cnx->esc($this->mdp)}";
            if (!$cnx->xeq($req)->ins($this))
                return false;
//            if (!password_verify($mdp, $this->mdp)) //this->mdp est maintenant le mdp crypté
//                return false;
            $_SESSION['id_user'] = $this->id_user;
            return true;
        }

        public static function getUserSession() {       //sur chaque page, permet de savoir si l'utilisateur est logué
            if (empty($_SESSION['id_user'])) //empty pose les questions si c'est défini (isset) et pas vide
                return null;
            $user = new User($_SESSION['id_user']);
            return $user->charger() ? $user : null;

            // $user = new User();
            // $user->id_user = $_SESSION['id_user'] ?? null;
            // return $user->charger() ? $user : null;
        }

    }
