<?php

    require_once '../framework/Connexion.php';

    class Fleur implements Databasable {

        public $id_fleur;
        public $nom;
        public $remarques;
        public $liens;


        public function __construct($id_fleur = null, $nom = null,$remarques = null, $liens = null) {
            $this->id_fleur = $id_fleur;
            $this->nom = $nom;
            $this->remarques = $remarques;
            $this->liens = $liens;

        }

        public function charger() {
            if (!$this->id_fleur)
                return false;
            $req = "SELECT * FROM fleur WHERE id_fleur ={$this->id_fleur}"; // requete SQL qui permet de recupérer un produit grace a son, id_produit
            return Connexion::getInstance()->xeq($req)->ins($this);
        }


        public function update() {
            if(!$this->id_fleur){
                return false;
            }
                $cnx = Connexion::getInstance();
                //si le produit a un id_produit alors on fait un UPDATE du produit en BDD
                $req = "UPDATE fleur SET nom={$cnx->esc($this->nom)},remarques={$cnx->esc($this->remarques)},liens={$cnx->esc($this->liens)} WHERE id_fleur={$this->id_fleur}";
                $cnx->xeq($req);
        }

        public function sauver() {
            $cnx = Connexion::getInstance();
           // si le produit n'a pas d id_produit alors on fait un INSERT du nouveau produit en BDD en passant tout les parametres
                $req = "INSERT INTO fleur VALUES (DEFAULT,{$cnx->esc($this->nom)},{$cnx->esc($this->remarques)}, {$cnx->esc($this->liens)})";
                $this->id_fleur = $cnx->xeq($req)->pk();
           // le DEFAULT de la requete INSERT permet d'utiliser l'auto incrementation de l'id_produit de la BDD
            return $this;
        }

        public function supprimer() {
            if (!$this->id_fleur)
                return false;
            $req = "DELETE FROM fleur WHERE id_fleur={$this->id_fleur}";
            return (bool) Connexion::getInstance()->xeq($req)->nb();
        }

        public function getCategorie() {
//            return (new Categorie($this->id_categorie))->charger();
        }

        public function refExiste() {   // permet d'interroger la BDD pour savoir si une ref existe deja , voir page editer
//            $cnx = Connexion::getInstance(); // appelle de variable global $db
//            $id_produit = $this->id_produit ? $this->id_produit : 0; // on peut enlever le $this->id_produit car le ternaire est inteligeant
//            $req = "SELECT * FROM produit WHERE ref={$cnx->esc($this->ref)} AND id_produit!={$id_produit}";
//            // requete SELECT permettant de savoir si une ref existe et que l'id produit nest pas le meme
//            // execution de la requete avec la methode query de PDO car cest une requete SELECT
//            //  setFetchMode en FETCH_CLASS afin de remplir une instance de la classe Produit en chargeant les propriété plus tard car
//            // on utilise FETCH_PROPS_LATE
//            return (bool) $cnx->xeq($req)->prem(__CLASS__);
        }

        public static function tous() {
            return Fleur::tab(1, 'nom');
        }

          public static function tab($where=1,$orderBy=1,$limit=null){
            $req = "SELECT * FROM fleur WHERE {$where} ORDER BY {$orderBy}". ($limit ? " LIMIT {$limit}" : '');
            return Connexion::getInstance()->xeq($req)->tab(__CLASS__);
        }
    }
    