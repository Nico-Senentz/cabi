<?php


Cfg::init();

class Cfg
{

    private static $initDone = false;
    public static $user = null;

    // Upload
    const TAB_EXT = [];
    const TAB_MIME = ['image/jpeg'];
    // Image
    const IMG_V_LARGEUR = 300;
    const IMG_V_HAUTEUR = 300;
    const IMG_P_LARGEUR = 450;
    const IMG_P_HAUTEUR = 450;
    // Session
    const SESSION_TIMEOUT = 300; // 5 minutes.

    private function __construct()
    {
        // Classe 100% statique
    }

    public static function init()
    {

        if (self::$initDone)
            return false;

        // auto-chargement des classes
        spl_autoload_register(function ($classe) {
            @include "../class/{$classe}.php";
        });
        spl_autoload_register(function ($classes) {
            @include "../framework/{$classes}.php";
        });
        // DSN
        Connexion::setDSN('nicosenentz_flower', '161829', 'Nicolas1986*');
        //Session
        session_set_save_handler(new Session(self::SESSION_TIMEOUT));
        session_start();
        self::$user = User::getUserSession();
        // Init done
        return self::$initDone = true;
    }

}

